# **Project Overview** #
## Premise ##
To install identical software configurations rapidly and consistently
with no interaction using the vendor-supplied packaging tools.  All
configuration happens during initial install, so that the system is ready at
the end of the *Assembly Line* to perform a certain assigned function at
first boot.  No additional customization or configuration should be
necessary after the system leaves the *Runtime Platform Factory*.

## What is a *Runtime Platform Factory*? ##
An environment that can rapidly assemble multiple *Runtime Platforms* from one or more *Runtime Chassis*.

## What is a *Runtime Chassis*? ##
A* Runtime Chassis* is a broader term for a software platform, in this case
it would represent the operating system components only.  This operating system would be installed directly on bare metal hardware or a new VM container.
To be considered a *Runtime Chassis*, the operating system must be
installed with a specific list of stock package components from the 
vendor using a vendor-supplied installation system aka *The Assembly Line*
(ie: Kickstart, Preseed, AutoYAST, etc.).  At this level, the system
contains no custom modifications other than disk layout
(and even the disk layout is tightly controlled).  A *Runtime Chassis* must be
able to be assembled quickly, generically, and non-interactively from a
vendor-supplied installation source on the *Assembly Line*.
A *Runtime Chassis* should boot into a running system with minimal software
installed.  Even though the *Runtime Chassis* is operable, it is intended to
serve as a blank frame, or chassis, to have additional software installed to
customize and complete the system.

## What is a *Runtime Platform*? ##
A *Runtime Platform* (RTP) is a *Runtime Chassis* with additional modifications
to make it specific for certain applications or entire sites.
*Runtime Chassis* models are derived from this.  Customizations are to be made
using pre-established tightly controlled "components" that are stored in 
package repos.  At this level, components are not host specific, but they may
be network or site specific.

## What is a *Runtime Chassis Model*? ##
A *Runtime Chassis Model* is a specific *Runtime Platform* to accomplish
specific tasks or meet specific host requirements.  Additonal components are
added or overlaid to complete the final end-user or production configuration.
These components consist of individual host configuration, specific software
customizations for that particular end user, etc.  These components are to be
installed on the *Assembly Line*.

## What is the *Assembly Line* quality control strategy? ##
**No manual component modification or customizations are to be made after first boot.**  Any missing
or faulty configuration must be addressed through package modifications and delivered
to the *Assembly Line* repos.  Systems that fail to meet quality requirements are not to be repaired
and deployed.  Instead, they are to be destroyed and a new *Runtime Chassis Model* (containing the 
appropriate improvements) should be assembled on the *Assembly Line* to take it's place.  After a system 
has been deployed into production, certified package updates can be pulled from the RTP repo if necessary
during appropriate maintenance intervals.  Package updates should drive **all** configuration changes
on the system.  No other mechanism, either deliberate or automatic, should modify the system in any
way.  Thus, system integrity can be verified with built-in packaging tools (ie: rpm -V) at any time.  This self-audit capability and design 
criteria is paramount and is incongruent to the typical "Puppet" or "Chef" strategy that is popular today.

## How are system updates and upgrades made? ##
Individual components that have passed quality control should be placed into
the *RTP* repo.  This way, the next *Latest Model* will automatically get the improvements
on the *Assembly Line* and existing systems can be upgraded as needed using 
the vendor's package upgrade procedures (ie: rpm -U or dpkg -i).
As part of an upgrade procedure, if possible, it is recommended to dispose of
the *Current Model* and deploy a *Latest Model* directly from *Assembly Line.*

## What is a *Current Model* and a *Latest Model*? ##
The term *model* is used as an example.  In this case is conceptually similar to *vehicle* models.  At least once
per year, a manufacturer comes out with a new *model* which contains improvements
over the previous model.  What we don't always see in the vehicular world is
that incremental improvements are often built into models of the same model
year.

## How are *Model* revisions applied? ##
When applied to the *Runtime Chassis*, each non-vendor supplied package
is versioned as follows: YYYYMMDDSN.  YYYY=Year, MM=Month, DD=Day, SN=Serial
number (for that day).  As soon as a new component is released to the
RTP repo, all *Runtime Platforms* and *Models* are considered "*Latest Models*" and
any systems built with the previous package level(s) are considered *Current
Models*.

## What about the *Oldest Model*? ##
The *Oldest Model* is a special model.  It is designed to stay at the oldest
OS level supported by the organization.  This model is to be used for compiling
and linking software, and the chosen OS version is the lowest common denominator.
This model is for development and testing purposes only and is not to be used in
production.

## How do vendor releases fit into the *Model* lineup? ##
Vendor releases are tracked the same way as internal releases.  Therefore
they will be assigned the same designation.  But when this designation is
applied to the OS version, it is not to be confused with *RTP* version.
For example, a *Latest RTP* version may be using the vendor supplied OS
version that the customer may label as *Current*.  It is the customer's
choice to determine what OS and software levels comprise the models being
built on the *Assembly Line*.

## TL;DR -- how do I use the software in this repo? ##
This repo contains SRPM (Source RPM packages).  Binary packages built
from the source packages here will be available in repos.  Links provided
shortly.

## What Linux versions are supported? ##
Binary packages are available in a public Yum repo for:

* RHEL 5 (ready), 6 (ready), 7 (pending systemd integration).
* CentOS 5 (ready), 6 (ready), 7 (pending systemd integration).

And in a public Apt repo for:

* Ubuntu Server LTS 12.04 (ready), 14.04 (ready), 16.04 (pending systemd integration).

## How do you support Ubuntu when there's only SRPMs here? ##
All packages in this project are directly derived from a single SRPM per
component.  This SRPM contains a spec file with special logic that will
build and run on the supported platforms.  In some cases, a single spec 
file may generate many configuration management subpackages.  All deb files
are generated using specialized *RTP* tools with the assistance of *alien(1)*.
Be assured, these deb files are high quality packages, designed specifically
for use on Ubuntu using hooks in the RPM build process.  This allows for
maximum code reuse across multiple Linux flavors while still affording the
ability to account for differences.